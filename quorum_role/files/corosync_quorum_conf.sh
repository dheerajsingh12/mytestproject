bash -c "cat <<EOF > /etc/corosync/corosync.conf
totem {
version: 2
cluster_name: mysql_cluster
transport: udpu
interface {
ringnumber: 0
Bindnetaddr: {{ hostvars[inventory_hostname]['ansible_host'] }}
broadcast: yes
mcastport: 5405
}
}
quorum {
provider: corosync_votequorum
device {
votes: 1
model: net
net {
tls: on
host: {{ hostvars[groups['quorum_group'][0]]['inventory_hostname'] }} 
algorithm: ffsplit
}
}
}
nodelist {
node {
ring0_addr: {{ hostvars[groups['zabbix_vm_primary_group'][0]]['inventory_hostname'] }}
name: {{ hostvars[groups['zabbix_vm_primary_group'][0]]['inventory_hostname'] }}
nodeid: 1
}
node {
ring0_addr: {{ hostvars[groups['zabbix_vm_secondary_group'][0]]['inventory_hostname'] }}
name: {{ hostvars[groups['zabbix_vm_secondary_group'][0]]['inventory_hostname'] }}
nodeid: 2
}
}
logging {
to_logfile: yes
logfile: /var/log/corosync/corosync.log
timestamp: on
}
EOF"
