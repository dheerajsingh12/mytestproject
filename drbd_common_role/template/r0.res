resource r0 {
  meta-disk internal;
  device /dev/drbd0;
  net {
    allow-two-primaries no;
    after-sb-0pri discard-zero-changes;
    after-sb-1pri discard-secondary;
    after-sb-2pri disconnect;
    rr-conflict disconnect;
  }
  on {{ hostvars[groups['zabbix_vm_primary_group'][0]]['inventory_hostname'] }} {
    disk {{ zabbix_disk }};
    address {{ hostvars[groups['zabbix_vm_primary_group'][0]]['ansible_host'] }}:7789;
  }
  on {{ hostvars[groups['zabbix_vm_standby_group'][0]]['inventory_hostname'] }} {
    disk {{ zabbix_disk }};
    address {{ hostvars[groups['zabbix_vm_standby_group'][0]]['ansible_host'] }}:7789;
  }
}
